# 2018
## Template Content Editor (TCE)

**Magento 2 Extension**

**Created by Day4Code, a subdivision of Day4**

***

## What is it?
Allows developers to create editable templates in plain html and then allowing the client to simply update the content within the template via a dynamic form. Built into 3 sections for various users:

***The code editor*** allows the developer to edit the code whenever they need, yet be hidden from clients as to not mistakenly edit it.

***The form editor*** allows the editor to edit the content of the template created by the developer, by editing the given fields

***The content preview*** allows anyone to view the rough end result of the content, including images, links etc.
NOTE: The preview doesn't have any styling applied to it.

## Where can I use it?
Currently TCE is attached to the following `textarea`s of the following pages:


`#category_form_description` on editing a Category.

`#product_form_description` on editing a Product.

`#product_form_short_description` on editing a Product.

`#cms_block_form_content` on editing a CMS Block.

`#cms_page_form_content` on edting a CMS Page.


## How does it works?
TCE looks through all the **root elements** within the `textarea` after converting it to a dom tree, for elements containg the `x-template` class. It will then process that element and its' children to create an editable form. For example:

***CODE***

```
<div class="x-template">
    <h1>You can edit me</h1>
</div>
<div class="container">
    <h1>You can't edit me</h1>
</div>
```
***RESULT***

![Basic example](https://bytebucket.org/day4/magento-2-template-content-editor/raw/b79de799e3b62410cdfeecaf5c9896546167031d/example_images/basic_example.png "Logo Title Text 1")

By default TCE will automatically create editable fields for the following dom elements automatically:

`h1,h2,h3,h4,h5,h6 and p` A simple text input box.

`a` 2 Input boxes for editing the `href` and text of the link, unless there are more dom elements within the link element.

`img` 3 Input boxes for editing the `src`, `alt`, `title`.

## Whats more?
TCE uses a mix of class names and `x-*` attributes to build up a more customizable form:

## XClasses

### Rich Text Editor
`<div class="x-content"></div>` creates a rich text editor that can be used for text content. This block allows basic lists, links and styled text.

### Arrange Elements
`<div class="x-arrange">...</div>` allows the editor to rearrange all chidren within the specified dom element.

## XAttributes

### Helpful Titles
Format: `x-name="{Name for this element}"`

`<a href="/article1" x-name="Article Link">Read Article 1<a>` creates a title above the input elements to help with structuring and letting the user know what this for.

### Toggle Classes
Format: `x-class="{property_name}[-{index}]: {class-to-toggle}; ..."`

***Simple toggle***

`x-class="hide_article: hidden; feature_article: is-featured;"` will create a checkbox for each property found (eg. 'Hide Article', 'Feature Article'), allowing the editor to toggle the class ('hidden', 'is-featured') on or off.

***Advanced toggle***

`x-class="color_scheme-1: blue-freedom; color_scheme-2: yellow-sun;"` will create a set of radio buttons, allowing the editor to only choose one of the available classes as active. eg:

***CODE***

```
<div class="x-template blue-freedom" x-class="color_scheme-1: blue-freedom; color_scheme-2: yellow-sun;">
    <div class="article" x-class="hide_article: hidden; feature_article: is-featured;">
        <h3>Article Title</h3>
    </div>
</div>
```
***RESULT***

![Classes example](https://bytebucket.org/day4/magento-2-template-content-editor/raw/b79de799e3b62410cdfeecaf5c9896546167031d/example_images/classes_example.png "Color scheme can be toggled between blue-freedom and yellow-sun")

### Repeater
`x-repeat="1:5"` allows the editor to dynamically duplicate the given element with all its' children and content. the attribute value is formatted as: `{index}:{max-n}` where max-n is the max number of times the element may be duplicated and index will just represent the 1-based index of every duplicated element. eg:

***CODE***

```
<div class="x-template articles">
    <a href="/article1" class="article" x-repeat="1:10">
        <h3>Article Title 1</h3>
        <img src="https://picsum.photos/601" alt="Random Image 1" />
    </a>
    <a href="/article2" class="article" x-repeat="2:10">
        <h3>Article Title 2</h3>
        <img src="https://picsum.photos/602" alt="Random Image 2" />
    </a>
</div>
```
***RESULT***

![Repeater example](https://bytebucket.org/day4/magento-2-template-content-editor/raw/b79de799e3b62410cdfeecaf5c9896546167031d/example_images/repeater_example.png "Element can be repeated upto 10 times in this example")


### Edit any attribute property (>2.2.6)
`x-attr="{attribute-name}:{type of value (text|image)}` allows the editor to update the values of attributes for the sepecified element. eg:

***CODE***

```
<div class="x-template">
    <button x-attr="title:text; onclick:text" title="You can change me" onclick="alert('you can even change this');"><p>I need to be in a p element to be editable</p></button>
</div>
```
***RESULT***

![Repeater example](https://bytebucket.org/day4/magento-2-template-content-editor/raw/b309501ebb70ce2d3fae14b92291fa2dbbfbace9/example_images/x-attr_example.png "Two attributes of a button available to change")


### Edit style property (>2.2.8)
`x-style="{style-name}:{type of value (text|image)}` allows the editor to change the values of the given style properties of the sepecified element. eg:

***CODE***

```
<div class="x-template">
    <div x-style="background-image:image; color:text;" style="color:#ffff00;background-repeat:no-repeat;background-size:cover;background-image:url(https://picsum.photos/343);">
        <h3>Coffee</h3>
        <div class="x-content"><p>Have a cup a day</p></div>
    </div>
</div>
```
***RESULT***

![Repeater example](https://bytebucket.org/day4/magento-2-template-content-editor/raw/b309501ebb70ce2d3fae14b92291fa2dbbfbace9/example_images/x-style_example.png "Both, background-image and color styles, can be updated by the editor.")


### Dynamic Slots (>2.4)
`x-slot="{slot_id}: {Label for Slot}"` adds a switch to toggle injecting a component inside the specified html.

NOTE: Do not add anyother html inside an empty slot as it will be overriden once the switch is toggled.

Current slots available for use are:

`tce_gallery` - A gallery using chocalat js as a lightbox, images can also be assigned links
`tce_banner` - A responsive banner using slick js
`tce_slider` - An image slider using both slick and lightbox

***CODE***

```
<div class="x-template">
    <div x-slot="x_banner: Hero Banner"></div>
    <div x-slot="x_gallery: Gallery"></div>
</div>
```


***

TCE is fully extensiable, meaning that new features are being added all the time and then can be used simply adding the appropriate attributes/classes.


## Updates

2.4.2 Critical fix. Fixes slot bug in Magento 2.

2.4.1 Added mobile preview and moved preview into iframe.

2.4.0 Added support for loading Slots and default template to pre-populate empty tce.

2.3.6 Critical fix. Fixes inline html entities from being convert to mulformed html.

2.3.5 Critical update. Fixes empty html entities from being converted to mulformed html.

2.3.0 Major update. Allow editing of multiple textareas. Style updates.

## Other
If you have 404 issues with files from the `ace` library, then add the following to `{magento_site}/pub/static/.htaccess`
```
<IfModule mod_rewrite.c>
    RewriteRule ^(.+)\/ace\/(.+)(?<!\.min)\.(js|css)$ $1/ace/$2.min.$3 [L]
</IfModule>
```


## Can I help?
Sure! Found a bug or want to extend TCE, create a pull request and I'll add your name to contributors.