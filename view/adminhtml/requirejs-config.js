/**
 * Copyright © 2017 Day4Code. All rights reserved.
 * See COPYING.txt for license details.
 */

var config = {
    map: {
        '*': {
            'quill': 'Day4Code_TemplateContentEditor/js/lib/quill/quill',
            'VEtce': 'Day4Code_TemplateContentEditor/js/template-content-editor',
            'ace': 'Day4Code_TemplateContentEditor/js/lib/ace'
        }
    }
};
