define(['./container','./contentArea','./textInput','./linkSet','./checkboxSet','./radioSet','./imageInput','./tableInput'],
function ( container ,   contentArea ,   textInput ,   linkSet ,   checkboxSet ,   radioSet ,   imageInput ,   tableInput ) {
    return {
        container: container,
        contentArea: contentArea,
        text: textInput,
        link: linkSet,
        checkbox: checkboxSet,
        radioSet: radioSet,
        image: imageInput,
        table: tableInput
    };
});