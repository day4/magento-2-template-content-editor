define(['https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js'],function (toastr) {
    if(!document.getElementById('tce-font')) {
        var link = document.createElement('link');
        link.id = 'toastr-css';
        link.rel = 'stylesheet';
        link.href = 'https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css';
        document.head.appendChild(link);
    }

    let T = function(m) {
        toastr.info(m);
    };

    T.success = toastr.success;
    T.info = toastr.info;
    T.warning = toastr.warning;
    T.error = toastr.error;

    return T;
});