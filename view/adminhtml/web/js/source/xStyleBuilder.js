define(['./createElement','./utils','./xSync','./container','./imageBrowser'],function (createEl, utils, xSync, container, imageBrowser) {
    return (opts, attvalue, classArr, domEl) => {
        let d = null;
        try {
            d = utils.parseXData(attvalue);
        } catch (e) {
            utils.toastr('xStyleBuild - JSON string failed to parse x-style value: '+domEl.nodeName);
            return false;
        }

        let style = domEl.getAttribute('style') || '';
        let styleClass = classArr.join(' ');
        let inputs = [];
        let styles = utils.parseXData(style);

        let updateDelay = null;
        let updateStyle = () => {
            clearTimeout(updateDelay);
            updateDelay = setTimeout(() => {
                style = '';
                Object.keys(styles).forEach(n => {
                    if (styles[n] && styles[n] !== 'inherit')
                        style += n+':'+styles[n]+';';
                });
                domEl.setAttribute('style', style);
                xSync.update(domEl);
            }, 500);
        };

        Object.keys(d).forEach(n => {
            if (!styles.hasOwnProperty(n)) styles[n] = 'inherit';
            switch(d[n]) {
                case 'text':
                    let el = createEl('input', {
                        type: 'text',
                        placeholder: n,
                        title: n,
                        class: styleClass + ' x-style',
                        value: styles[n] !== 'inherit' ? styles[n] : ''
                    });
                    el.addEventListener('input', function (evt) {
                        styles[n] = el.value || 'inherit';
                        updateStyle();
                    });
                    inputs.push(el);
                break;
                case 'image':
                    let lbl = 'Set '+n;
                    let none = 'none';
                    if (styles[n] && styles[n] !== 'inherit') {
                        lbl = 'Change '+n;
                        none = '';
                    }
                    let btn = createEl('button', {class: 'mage-btn white-btn'}, lbl);
                    let preview = createEl('div', {
                        class: 'img-preview ' + none,
                        style: n + ': ' + styles[n]
                    });
                    btn.onclick = (e) => {
                        imageBrowser(function(value) {
                            if (value) {
                                styles[n] = 'url('+utils.imageUrls(value)+')';
                                preview.style[n] = styles[n];
                                preview.classList.remove('none');
                            } else {
                                styles[n] = '';
                                preview.style[n] = styles[n];
                                preview.classList.add('none');
                            }
                            updateStyle();
                        });
                    };
                    inputs.push(createEl('span', {class: styleClass + ' x-style x-image'}, [btn, preview]));
                break;
            }
        });

        if (inputs.length) {
            classArr.push('x-styles');
            opts.push(container(classArr, inputs));
        }

        return true;
    }
});
