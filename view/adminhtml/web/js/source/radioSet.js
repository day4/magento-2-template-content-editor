define(['./createElement','./utils','./xSync'],function (createEl, utils, xSync) {
    return (classArr, domEl, name, values) => {
        let nodes = [createEl('h5', {class: "label"}, name.replace(/\_/g, ' '))];

        let d = null;
        let onCBChange = (evt) => {
            let curCB = evt.target;
            if (curCB.checked) {
                nodes.forEach(_cb => {
                    if (_cb !== curCB) {
                        _cb.checked = false;
                        domEl.classList.remove(_cb.value);
                    }
                });
                if (curCB.value !== 'default') domEl.classList.add(curCB.value);
            } else domEl.classList.remove(curCB.value);
            xSync.update(domEl);
        };

        values.forEach((val, i) => {
            let ID = classArr[0] + '-rd-' + utils.getId();
            let cb = createEl('input', {
                type: 'checkbox',
                id: ID,
                name: name,
                value: val,
                class: 'mage-cb'
            });
            if (domEl.classList.contains(val)) d = cb;
            if (val == 'default' && !d) d = cb;

            cb.addEventListener('change', onCBChange);

            nodes.push(cb);
            nodes.push(createEl('label', {for: cb.id}, val.replace(/\-/g, ' ')));
        });

        if (d) d.checked = true;
        else if (nodes.length) {
            nodes[0].checked = true;
            nodes[0].dispatchEvent(new Event('change'));
        }

        return createEl('span', {class: "radio-set"}, nodes);
    };
});
