define(['./createElement', './container', './textInput'],function (createEl, container, textInput) {
    return (classArr, domEl, children) => {
        classArr.push('link-set');
        let hrefEl = textInput(['link-href'], domEl, '⚓ Link Href', 'href');
        if (Array.isArray(children) && children.length) {
            children.unshift(hrefEl);
            return container(classArr, children);
        } else return createEl('span', {class: classArr.join(' ')}, [
            textInput(['link-test'], domEl, 'Link Text'),
            hrefEl
        ]);
    };
});