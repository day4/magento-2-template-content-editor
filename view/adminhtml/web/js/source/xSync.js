define(['jquery','./utils'],function ($,utils) {
    let SCOPES = {};
    let OPTIONS = {};

    function xSync(_codeEl) {
        let id = utils.getId();
        let codeEl = _codeEl;
        let liveEl = null;
        let XMLDOM = null;
        let updateDelay = null;

        function __sync(html, forceUpdate) {
            codeEl.value = utils.toMageCode(html);
            /* Dispatch event telling magento to update */
            var evt = new Event('change');
            evt._tce_invoked = !forceUpdate;
            codeEl.dispatchEvent(evt);
        }

        this.getId = () => {return id;};

        /* Code Element */
        this.getCodeEl = () => {return codeEl;};

        /* XMLDOM Object */
        this.setDOM = (_dom) => {
            XMLDOM = _dom;
            XMLDOM.ownerDocument.__xSyncId = id;
        };

        this.getDOM = () => {return XMLDOM;};

        /* Preview Element */
        this.setPreviewEl = (_liveEl) => {liveEl = _liveEl;};
        this.getPreviewEl = () => {return liveEl;};

        /* Options */
        this.setOpts = (_opts) => {
            if (opts) {
                opts = _opts;
                if (opts.hasOwnProperty('mediaPath')) utils.setMediaPath(opts.mediaPath);
            }
        };

        this.getOpts = () => {return opts;};

        let failed = 0;
        this.updatePreview = (html) => {
            /**
             * Run preview in iframe
             * TODO Merge view with view from desitination source
             */
            try {
                iframe = liveEl.getElementsByTagName("iframe")[0];
                iframe.contentWindow.document.open();
                iframe.contentWindow.document.write(
                    `<!DOCTYPE html>
                    <html>
                    <head>
                        <meta charset="utf-8">
                        <meta name="viewport" content="width=device-width, initial-scale=1">
                        <title>TCE Previewer</title>
                        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
                    </head>
                    <body>`
                    +html+
                    `</body>
                    </html>`
                );
                iframe.contentWindow.document.close();
                failed = 0;
            } catch (err) {
                liveEl.innerHTML = '<iframe></iframe>';
                if (failed++ < 3)
                    this.updatePreview(html);
                else
                    utils.toastr.error(err);
            }
        }

        this.update = (forceUpdate) => {
            clearTimeout(updateDelay);
            updateDelay = setTimeout(() => {
                if (XMLDOM) {
                    let html = utils.xmlToHtml(XMLDOM.innerHTML);
                    html = utils.imageUrls(html);
                    this.updatePreview(html);

                    __sync(html, forceUpdate);
                }
            }, 250);
        };

        this.destroy = () => {
            delete(SCOPES[id]);
            delete(SCOPES[codeEl.id]);
        };

        SCOPES[id] = SCOPES[codeEl.id] = this;
    }

    return {
        new: (_codeEl) => {
            if (!_codeEl || !_codeEl.id) throw 'Invalid arguments in xSync.new';
            if (SCOPES[_codeEl.id]) throw 'Already initialized';
            return new xSync(_codeEl);
        },
        setOpts: (opts) => {
            if (typeof opts !== 'object') throw 'Invalid argument type in Sync.setOpts';
            Object.keys(opts).forEach(key => {
                if (OPTIONS.hasOwnProperty(key)) console.log('xSync Options overriding value set for ['+key+']');
                OPTIONS[key] = opts[key];
            });

            if (OPTIONS.hasOwnProperty('mediaPath')) utils.setMediaPath(OPTIONS.mediaPath);
        },
        getOpts: () => {return OPTIONS;},
        update: (XMLDOM, forceUpdate) => {
            if (!XMLDOM || !XMLDOM.ownerDocument.__xSyncId || !SCOPES[XMLDOM.ownerDocument.__xSyncId]) {
                console.debug('Missing/Invalid XMLDOM argument in xSync.update');
            } else SCOPES[XMLDOM.ownerDocument.__xSyncId].update(forceUpdate);
        }
    };
});
