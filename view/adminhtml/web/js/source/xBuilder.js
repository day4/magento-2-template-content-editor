define(['./xClassBuilder','./xRepeatBuilder','./xAttributeBuilder','./xStyleBuilder','./xArrangeBuilder','./xSlotBuilder'],
function ( xClassBuilder ,   xRepeatBuilder ,   xAttributeBuilder ,   xStyleBuilder ,   xArrangeBuilder,   xSlotBuilder) {
    return (classArr, domEl) => {
        let opts = domEl._xOpts || [];

        if (attvalue = domEl.getAttribute('x-class')) xClassBuilder(opts, attvalue, classArr, domEl);

        if (attvalue = domEl.getAttribute('x-repeat')) xRepeatBuilder(opts, attvalue, classArr, domEl);

        if (attvalue = domEl.getAttribute('x-attr')) xAttributeBuilder(opts, attvalue, classArr, domEl);

        if (attvalue = domEl.getAttribute('x-style')) xStyleBuilder(opts, attvalue, classArr, domEl);

        if (attvalue = domEl.getAttribute('x-slot')) xSlotBuilder(opts, attvalue, classArr, domEl);

        if (attvalue = domEl.classList.contains('x-arrange')) xArrangeBuilder(opts, attvalue, classArr, domEl);

        return opts;
    };
});