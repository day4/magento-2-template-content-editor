define(['jquery', 'quill', './createElement', './utils', './xSync', './container'],function ($, Quill, createEl, utils, xSync, container) {
    let QUILL_OPTIONS = {
        modules: {
            clipboard: true,
            toolbar: [
                ['link', 'bold', 'italic', 'underline', 'strike'],
                [{ 'list': 'ordered'}, { 'list': 'bullet' }],
                [{ 'script': 'sub'}, { 'script': 'super' }],
                [{ 'indent': '-1'}, { 'indent': '+1' }]
            ]
        },
        placeholder: 'P',
        theme: 'snow'
    };
    return (classArr, domEl, placeholder) => {
        classArr.push('textarea');
        let el = createEl('div', {
            title: domEl.name,
            class: classArr.join(' ')
        });

        setTimeout(()=>{
            let q = new Quill(el, QUILL_OPTIONS);
            q.clipboard.dangerouslyPasteHTML(0, utils.desanitize(domEl.innerHTML));

            let delay = null;
            q.on('text-change', function(delta, oldDelta, source) {
                clearTimeout(delay);
                delay = setTimeout(() => {
                    let html = q.container.firstChild.innerHTML;
                    if (html == '<p><br></p>') html = '';
                    html = html.replace(/<(b|h)r>/g,`<$1r/>`).replace(/&nbsp;/g,'');
                    domEl.innerHTML = html;
                    xSync.update(domEl);
                }, 300);
            });

            /* Toggle quill toolbar */
            let qt = q.theme.modules.toolbar;
            q.on('selection-change', function(range, oldRange, source) {
                if (range) $(qt.container).show();
                else $(qt.container).hide();
            });
            $(qt.container).hide();
        },100);

        return el;
    };
});
