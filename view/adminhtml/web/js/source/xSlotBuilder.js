/**
 * Allows for dynamic injection of html snippets from TCE Server
 */
define(['jquery', './createElement', './utils', './xSync', './xApi', './xMessagePrompt', './checkboxSet'],function ($, createEl, utils, xSync, xApi, xMessagePrompt, checkboxSet) {
    return (opts, attvalue, classArr, domEl) => {
        let d = null;
        try {
            d = utils.parseXData(attvalue, false, true);
        } catch (e) {
            utils.toastr('xSlotBuild - JSON string failed to parse x-slot attribute value: '+domEl.nodeName);
            return false;
        }

        Object.keys(d).forEach(n => {
            let label = d[n];
            let ID = classArr[0] + '-' + utils.getId();
            let el = createEl('input', {
                type: 'checkbox',
                title: 'Enable/Disable '+label,
                value: n,
                class: 'mage-cb '+classArr.join(' '),
                id: ID
            });

            let lbl = createEl('label', { for: el.id }, label);

            if (domEl.classList.contains(n)) el.checked = true;

            el.addEventListener('change', function (evt) {
                if (el.checked) {
                    domEl.classList.add(n);
                    xApi.findSlot(n).then((xhtml) => {
                        $(domEl).html(xhtml);
                        xSync.update(domEl, true);
                    }).catch(err => {
                        el.checked = false;
                    });
                } else {
                    xMessagePrompt(
                        "<h3>Disable Slot?</h3><p>Disabling the slot will remove any content or changes you did within the slot.</p><p>Are you sure?</p>",
                        (v) => {
                            if (v) {
                                domEl.classList.remove(n);
                                domEl.innerHTML = '';
                                xSync.update(domEl, true);
                            } else {
                                el.checked = true;
                            }
                        },
                        [{value: true, label: 'Yes'}, {value: false, label: 'Cancel'}]
                    );
                }
            });


            let collapse = createEl('span', {class: 'tce-slot'}, [
                createEl('span', {class: 'checkbox-set'}, [el, lbl])
            ]);
            let toggle = checkboxSet([classArr[0], 'cb-vertical'], collapse, '', 'tce-slot-visible')
            toggle.style.position='absolute';
            toggle.style.right='4px';
            toggle.style.top='4px';
            collapse.appendChild(toggle);
            opts.push(collapse);
        });
        return opts;
    }
});
