define(['./createElement','./utils','./xSync','./container','./textInput','./imageBrowser'],function (createEl, utils, xSync, container, textInput, imageBrowser) {
    return (opts, attvalue, classArr, domEl) => {
        let d = null;
        try {
            d = utils.parseXData(attvalue);
        } catch (e) {
            utils.toastr('xAttributeBuild - JSON string failed to parse x-attr value: '+domEl.nodeName);
            return false;
        }

        let attrs = [];

        Object.keys(d).forEach(n => {
            switch(d[n]) {
                case 'text':
                    attrs.push(textInput(['x-attr'], domEl, n, n));
                break;
                case 'image':
                    let btn = createEl('button', {class:'x-attr mage-btn'}, 'Set '+n);
                    btn.onclick = (e) => {
                        imageBrowser(function(value) {
                            domEl.setAttribute(n, value);
                            xSync.update(domEl, true);
                        });
                    };
                    attrs.push(btn);
                break;
            }
        });

        if (attrs.length)
            opts.push(container(classArr, attrs));
        return true;
    }
});