/**
 * Usage:
    xMessagePrompt(
        "<h3>Hello World</h3><p>This is a shout to the world</p>",
        (v) => {console.info('Pressed ',v)},
        [{value: true, label: 'Cool'}, {value: false, label: 'Not Cool'}, {value: 'cancel'}]
    );
 *
 */
define(['jquery','./utils','./createElement'], function ($, utils, createEl) {
    let ID = 'tce-msg-prompt-popup';
    let popupEl = document.getElementById(ID);

    let callback;
    let closePopup = (res, data) => {
        $(popupEl).fadeOut(()=>{
            $(popupEl).find('.tce-popup-container .row').html('');
            callback && callback(res, data)
            callback = null;
        });
    };

    if (!popupEl) {
        let backdropEl = createEl('div', {class: 'tce-popup-backdrop tce-popup-backdrop-window'});
        backdropEl.onclick = (e) => { closePopup(); };

        popupEl = createEl('div', {class: 'tce tce-popup', id: ID}, [
            backdropEl,
            createEl('div', {class: 'tce-popup-container'}, [
                createEl('div', {class: 'row content'}),
                createEl('div', {class: 'row actions'})
            ])
        ]);

        document.body.appendChild(popupEl);
    }

    return (_msg, _callback, _opts, _input) => {
        if (callback) _callback();
        callback = _callback;

        $(popupEl).find('.tce-popup-container .row').html('');
        $(popupEl).find('.tce-popup-container .row.content').append('<div>'+_msg+'</div>');

        if (_input) {
            _input = $('<textarea placeholder="'+_input+'"/>');
            $(popupEl).find('.tce-popup-container .row.content').append(_input);
        }

        _opts && _opts.forEach(btn => {
            let new_btn = createEl('button', {class: 'mage-btn white-btn'+(btn.class ? btn.class : '')}, btn.label || btn.value);
            new_btn.onclick = (e) => {
                closePopup(btn.value, _input && _input.val());
            };

            $(popupEl).find('.tce-popup-container .row.actions').append(new_btn);
        });

        $(popupEl).fadeIn(400, () => {});
    };
});
