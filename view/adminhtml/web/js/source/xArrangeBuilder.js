define(['./createElement','./utils', './xSync'],function (createEl, utils, xSync) {

    return (opts, attvalue, classArr, domEl) => {
        let l = domEl.children.length;
        if (l) {
            let order = [];
            for (let i=0; i < l; i++) {
                let c = domEl.children[i];
                let cOpts = c._xOpts || [];
                c.setAttribute('x-order', i);

                let upbtn = createEl('button', {class:'mage-btn up', title: 'Move up'}, '');
                let downbtn = createEl('button', {class:'mage-btn down', title: 'Move down'}, '');

                upbtn.addEventListener('click', (evt) => {
                    if (c.previousSibling) {
                        domEl.insertBefore(c, c.previousSibling);
                        xSync.update(domEl, true);
                    }
                });

                downbtn.addEventListener('click', (evt) => {
                    if (c.nextSibling) {
                        domEl.insertBefore(c.nextSibling, c);
                        xSync.update(domEl, true);
                    }
                });

                cOpts.push(createEl('span', {class: "arrange-set"}, [upbtn, downbtn]));
                c._xOpts = cOpts;
            }

            return true;
        }
        return false;
    }
});