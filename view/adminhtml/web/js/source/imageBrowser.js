/**
 * Native Image Browser
 * 1. Calls the function of the set Image Browser
 * 2. Checks a hidden input for a change in value
 * 3. Executes the callback function with the new value
 */

define(['jquery','./utils','./createElement','./xSync'], function ($, utils, createEl, xSync) {
    let listener = null;
    let image_selector = document.getElementById('tce-image-chooser');
    if (!image_selector) {
        image_selector = createEl('input', {class: 'tce', id: 'tce-image-chooser'});
        document.body.appendChild(image_selector);
    }
    return (_callback) => {
        clearInterval(listener);
        let opts = xSync.getOpts();
        if (!opts.imageBrowser) { console.toastr('No Image Browser method found!'); return; }
        let initVal = image_selector.value = 'tce-empty';
        opts.imageBrowser();
        listener = setInterval(() => {
            if(image_selector.value !== initVal) {
                if (mcode = utils.urlToMageCode(image_selector.value)) {
                    _callback(utils.fromMageCode(mcode, true));
                } else {
                    _callback(utils.fromMageCode(image_selector.value));
                }
                _callback = ()=>{};
                clearInterval(listener);
            }
        }, 250);
    };
});
