define(['./createElement','./utils','./xSync', './xMessagePrompt'],function (createEl, utils, xSync, xMessagePrompt) {

    const tceTable = {
        click: (e, curTD) => {
            if (curTD && e.target !== curTD) {
                curTD.removeAttribute('contentEditable');
                curTD = null;
            }
            if (e.target.tagName === 'TD') {
                curTD = e.target;
                curTD.setAttribute('contentEditable', true);
                curTD.focus();
            }
            return curTD;
        },
        newRow: (table, rI) => {
            var cols = table.rows.length ? table.rows[0].cells.length : 3;
            var row = table.insertRow(rI);

            for (var cI = 0; cI < cols; cI++) {
                row.insertCell();
            }
        },

        newCol: (table, cI) => {
            for (var rI = 0; rI < table.rows.length; rI++) {
                table.rows[rI].insertCell(cI);
            }
        },

        delRow: (table, rI) => {
            rI = isNaN(rI) ? table.rows.length - 1 : rI*1;
            table.deleteRow(rI);
        },

        delCol: (table, cI) => {
            cI = isNaN(cI) ? table.rows[0].cells.length - 1 : cI*1;

            for (var rI = 0; rI < table.rows.length; rI++) {
                table.rows[rI].deleteCell(cI);
            }
        }
    }

    /**
     * domEl shall be a table element
     */
    return (classArr, domEl) => {

        let curTD = null;
        let tbl = createEl('table', {}, domEl.innerHTML);

        let update = function(resetTools) {
            domEl.innerHTML = tbl.innerHTML.replace(/(contenteditable="true"|&nbsp;)/g,'');
            xSync.update(domEl);
            if (resetTools) {
                optsEl.style.left = 'initial';
                optsEl.style.top = 'initial';
            }
        }

        /* Setup actions and buttons */
        let pasteBtn = createEl('button', {class:'paste', title: 'From clipboard'}, '');
        pasteBtn.onclick = (e) => {
            xMessagePrompt(
                "<h3>Replace table?</h3><p>Warning: The current table will be replaced!</p>",
                (v, data) => {
                    if (v) {
                        var rows = data.split("\n");
                        tbl.innerHTML = '';

                        rows.forEach(row => {
                            var cells = row.split("\t");
                            tbl.appendChild(createEl('tr', {}, cells.map(cell => createEl('td', {},cell))))
                        });
                        update();
                    }
                },
                [{value: true, label: 'Ok'}, {value: false, label: 'Cancel'}],
                'Paste table in here.'
            );


            return false;
        };

        /* Setup actions and buttons */
        let newRow = createEl('button', {class:'add row', title: 'Add Row'}, '');
        let delRow = createEl('button', {class:'del row', title: 'Remove Row'}, '');
        let newCol = createEl('button', {class:'add col', title: 'Add Col'}, '');
        let delCol = createEl('button', {class:'del col', title: 'Remove Col'}, '');
        let optsEl = createEl('span', {class: 'options'}, [pasteBtn, newRow, delRow, newCol, delCol]);

        newRow.onclick = (e) => {
            tceTable.newRow(tbl, curTD ? curTD.parentNode.rowIndex+1 : undefined);
            update(true);
            return false;
        };

        delRow.onclick = (e) => {
            tceTable.delRow(tbl, curTD ? curTD.parentNode.rowIndex : undefined);
            update(true);
            return false;
        };

        newCol.onclick = (e) => {
            tceTable.newCol(tbl, curTD ? curTD.cellIndex+1 : undefined);
            update(true);
            return false;
        };

        delCol.onclick = (e) => {
            tceTable.delCol(tbl, curTD ? curTD.cellIndex : undefined);
            update(true);
            return false;
        };

        tbl.addEventListener("click", e => {
            curTD = tceTable.click(e, curTD);
            if (curTD) {
                optsEl.style.left = curTD.offsetLeft+2+'px';
                optsEl.style.top = (curTD.offsetTop+curTD.offsetHeight+2)+'px';
            } else {
                optsEl.style.left = 'initial';
                optsEl.style.top = 'initial';
            }
        }, false);

        let delay = null;
        tbl.addEventListener("input", function() {
            clearTimeout(delay);
            delay = setTimeout(update, 50);
        }, false);

        return createEl('div', {class: classArr.join(' ') + ' tce-table'}, [
            tbl, optsEl
        ]);
    };
});