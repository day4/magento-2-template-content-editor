define(['jquery', './createElement', './utils', './xSync'],function ($, createEl, utils, xSync) {
  return (opts, attvalue, classArr, domEl) => {
      var $el = $(domEl);
      var d = null;
      var name = '';
      var index = 1;
      var limit = 1;
      var type = domEl.nodeName;
      try {
        d = utils.parseXData(attvalue, true, true);
        if (d.length == 2) {
          index = d[0]*1;
          limit = d[1]*1;
        } else if (d.length == 3) {
          name = d[0];
          index = d[1]*1;
          limit = d[2]*1;
        } else throw "Need 2 or 3 arguments to continue";

        if (isNaN(index) || isNaN(limit)) throw "Index & Limit must be number";
        if (index < 1 || d[1] < 1) throw "Index & Limit must be greater than 0";
        if (index > d[1]) throw "Index cant be more than limit";
      } catch (e) {
        utils.toastr('xRepeatBuild - JSON string failed to parse x-repeat attribute value: '+domEl.nodeName+' ('+e+')');
        return false;
      }

      name = name.length ? name + '-' : name;

      let abtn = createEl('button', {class:'mage-btn add', title: 'Add new'}, '');
      let rbtn = createEl('button', {class:'mage-btn remove', title: 'Remove'}, '');

      function getList($parent) {
        if (!name) return $parent.children(type+"[x-repeat]");
        return $parent.children(type+"[x-repeat^='"+name+"']");
      }

      function organize($set) {
        $set.each((i, el) => {
          el.setAttribute('x-repeat', name+(i+1)+':'+limit);
        });
        xSync.update(domEl, true);
      };

      abtn.onclick = (e) => {
        var $parent = $el.parent();
        let $c = getList($parent);
        if ($c.length < limit) {
          let $clone = $el.clone();
          $el.after($clone);
          $c = getList($parent);
          organize($c);
        } else utils.toastr("Limit reached, can't add more elements");
        return false;
      };

      rbtn.onclick = (e) => {
        var $parent = $el.parent();
        let $c = getList($parent);
        if ($c.length > 1) {
          $el.remove();
          $c = getList($parent);
          organize($c);
        } else utils.toastr("Can't remove all elements, must at least have one");
        return false;
      };

      opts.push(createEl('span', {class: "repeater-set"}, [abtn, rbtn]));

      return true;
  }
});
