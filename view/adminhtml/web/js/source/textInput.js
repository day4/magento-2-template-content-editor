define(['./createElement','./utils','./xSync'],function (createEl, utils, xSync) {
    return (classArr, domEl, placeholder, attr) => {
        let el = createEl('input', {
            type: 'text',
            placeholder: placeholder,
            title: placeholder,
            name: domEl.name,
            class: classArr.join(' ')
        });

        el.value = attr ? domEl.getAttribute(attr) : utils.desanitize(domEl.innerHTML, true);

        el.addEventListener('input', function (evt) {
            if (attr)
                domEl.setAttribute(attr, el.value);
            else
                domEl.innerHTML = utils.sanitize(el.value, true);
            xSync.update(domEl);
        });

        return el;
    };
});