define(['./createElement','./utils','./checkboxSet','./radioSet'],function (createEl, utils, checkboxSet, radioSet) {
    return (opts, attvalue, classArr, domEl) => {
        let d = null;
        try {
            d = utils.parseXData(attvalue, false, true);
        } catch (e) {
            utils.toastr('xClassBuild - JSON string failed to parse x-class attribute value: '+domEl.nodeName);
            return false;
        }

        Object.keys(d).forEach(n => {
            let v = d[n];
            if (Array.isArray(v))
                opts.push(radioSet(classArr, domEl, n, v));
            else
                opts.push(checkboxSet(classArr, domEl, n, v));
        });
        return true;
    }
});
