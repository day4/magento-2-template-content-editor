define(['jquery','./utils','./createElement', 'ace/ace'], function ($, utils, createEl, ace) {
    let aceEditor = null;
    let popupEl = document.getElementById('tce-code-popup');

    let onSave = null;
    let onCancel = null;
    let onEnd = null;

    let init = () => {
        aceEditor = ace.edit('tce-code-editor');
        aceEditor.$blockScrolling = Infinity;
        aceEditor.session.setMode('ace/mode/html');
    };

    let closePopup = () => {
        $(popupEl).fadeOut();
        aceEditor.setValue('');
        if (typeof onEnd === 'function') onEnd(aceEditor.getValue());
    };

    if (!popupEl) {
        let codeEl = createEl('div', {class: 'row', id: 'tce-code-editor', style: 'flex-grow: 1'});

        let backdropEl = createEl('div', {class: 'tce-popup-backdrop'});
        backdropEl.onclick = (e) => {
            if (typeof onCancel === 'function') onCancel();
            closePopup();
        };

        let toastr = createEl('p', {style:'display:none'}, 'Invalid HTML syntax, could not update.');

        let cancel_btn = createEl('button', {class:'mage-btn white-btn'}, 'Cancel');
        cancel_btn.onclick = (e) => {
            if (typeof onCancel === 'function') onCancel();
            closePopup();
        };

        let save_btn = createEl('button', {class:'mage-btn'}, 'Save');
        save_btn.onclick = (e) => {
            if (typeof onSave === 'function' && onSave(aceEditor.getValue()))
                closePopup();
            else {
                $(toastr).show();
                setTimeout(()=>{
                    $(toastr).hide();
                },4000);
            }
        };

        popupEl = createEl('div', {class: 'tce-popup', id: 'tce-code-popup', style: 'display:none'}, [
            backdropEl,
            createEl('div', {class: 'tce-popup-container'}, [
                codeEl,
                createEl('div', {class: 'row actions'}, [
                    toastr, cancel_btn, save_btn
                ])
            ])
        ]);
        document.body.appendChild(popupEl);
    }

    return (_value, _onSave, _onCancel, _onEnd) => {
        if (!aceEditor) init();
        $(popupEl).fadeIn(400, () => {
            aceEditor.setValue(_value);
            aceEditor.resize();
        });
        onSave = _onSave;
        onCancel = _onCancel;
        onEnd = _onEnd;
    };
});
