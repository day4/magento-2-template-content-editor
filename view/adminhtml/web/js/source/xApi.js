define(['jquery', './toastr'],function ($, toastr) {
    const API_TOKEN = 'ac983724adbf2bd2b453cc8aa35214';

    function buildSlot(slot, isTemplate) {
        var xhtml = `<div class="x-template ${slot.name}">\n`;
        if (!slot.styles && isTemplate) slot.styles = '/* Enter your styles here */';
        if (!slot.script && isTemplate) slot.styles = '/* Enter your script here */';
        if (slot.imports) xhtml += slot.imports;
        if (isTemplate || slot.styles) xhtml += `<style>/*<![CDATA[*/\n\t${slot.styles}\n/*]]>*/\n</style>\n\n`;
        xhtml += ('\t'+slot.content).replace('\n','\n\t');
        if (isTemplate || slot.script) xhtml += `\n<script type="text/javascript">\n\t/*<![CDATA[*/\n\t(function($) {\n\t$.noConflict();\n\t${slot.script}\n\t})(jQuery);\n\t/*]]>*/\n</script>\n`;
        xhtml += '</div>';
        return xhtml;
    }

    return {
        findSlot: (_name, isTemplate) => {
            return new Promise((resolve, reject) => {
                $.get('https://tce.day4.live/api/collections/get/Slots', {
                    filter: {
                        name: _name
                    },
                    token: API_TOKEN
                }, function(result) {
                    if (result && result.total) resolve(buildSlot(result.entries[0], isTemplate));
                    else reject('No slots found for '+_name);
                }).fail(reject);
            });
        }
    }
});