define(['./createElement','./utils','./xSync'],function (createEl, utils, xSync) {
    return (classArr, domEl, name, value) => {
        let ID = classArr[0] + '-' + utils.getId();
        let el = createEl('input', {
            type: 'checkbox',
            title: domEl.name,
            value: value,
            class: 'mage-cb '+classArr.join(' '),
            id: ID
        });
        ;

        let lbl = createEl('label', {
            for: el.id }, name.replace(/\_/g, ' '));

        if (domEl.classList.contains(value)) el.checked = true;

        el.addEventListener('change', function (evt) {
            if (el.checked)
                domEl.classList.add(value);
            else domEl.classList.remove(value);

            xSync.update(domEl);
        });

        return createEl('span', {class: 'checkbox-set'}, [el, lbl]);
    };
});
