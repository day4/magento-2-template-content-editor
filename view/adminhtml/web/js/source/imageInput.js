define(['./utils', './createElement', './xSync', './container', './textInput', './imageBrowser'],function (utils, createEl, xSync, container, textInput, imageBrowser) {
    return (classArr, domEl, placeholder) => {
        let title = textInput(['image-title'], domEl, 'Image Title', 'title');
        let alt = textInput(['image-alt'], domEl, 'Image Alt Text', 'alt');
        let src = textInput(['image-src'], domEl, 'Image Src', 'src');
        let btn = createEl('button', {class:'mage-btn'}, 'Choose Image');

        let imgPreview = document.getElementById('tce-preview-image');
        if (!imgPreview) {
            imgPreview = createEl('img', {id:'tce-preview-image'});
            document.body.appendChild(imgPreview)
        }

        btn.onclick = (e) => {
            imageBrowser(function(value) {
                src.value = value;
                domEl.setAttribute('src', value);
                xSync.update(domEl);
            });
            return false;
        };

        classArr.push('image-container');

        let mouseTrap = container(classArr, [
            title, alt, src, btn
        ])

        var mouseMoves = (event) => {
            imgPreview.style.left = (event.clientX+1)+'px';
            imgPreview.style.top = (event.clientY+1)+'px';
        };

        mouseTrap.addEventListener("mouseover", (event) => {
            if (src.value) {
                imgPreview.src = utils.imageUrls(src.value);
                mouseMoves(event);
                imgPreview.classList.add('active');
                document.addEventListener('mousemove', mouseMoves);
            }
            return false;
        });

        mouseTrap.addEventListener('mouseout', (event) => {
            document.removeEventListener('mousemove', mouseMoves);
            imgPreview.classList.remove('active');
            return false;
        });

        return mouseTrap;
    };
});