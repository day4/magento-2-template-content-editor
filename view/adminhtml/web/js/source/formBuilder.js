define(['jquery', './utils','./createElement', './formElements', './xBuilder'], function ($, utils, createEl, formEls, xBuilder) {
    let _formBuilder = (dom, level) => {
        var els = [];
        if (dom) {
            $.each(dom.children, (i,domEl) => {
                if (level || domEl.classList.contains('x-template')) {
                    let name = domEl.nodeName.toUpperCase();
                    if (['SCRIPT', 'STYLE', 'LINK', 'META'].indexOf(name) > -1) return;
                    let nextLevel = level+'-'+i;
                    let cla = ['tce-'+nextLevel];
                    let opts = xBuilder(cla, domEl);
                    if (xName = domEl.getAttribute('x-name')) opts.unshift(createEl('h5', {class:'title'}, xName));
                    let el = false;
                    switch(name) {
                        case 'A':
                            el = formEls.link(cla, domEl, _formBuilder(domEl, nextLevel));
                        break;
                        case 'P':
                        case 'H1':
                        case 'H2':
                        case 'H3':
                        case 'H4':
                        case 'H5':
                        case 'H6':
                            el = formEls.text(cla, domEl, name);
                        break;
                        case 'IMG':
                            el = formEls.image(cla, domEl, name);
                        break;
                        case 'TABLE':
                            el = formEls.table(cla, domEl, name);
                        break;
                        default:
                            if (domEl.classList.contains('x-content')) {
                                el = formEls.contentArea(cla, domEl, name);
                            } else {
                                cla.push('container');
                                let _cEls = _formBuilder(domEl, nextLevel);
                                if (_cEls.length)
                                    el = formEls.container(cla, _cEls);
                                else if (opts.length) {
                                    el = formEls.container(['set','container','has-opts'], opts);
                                    opts = [];
                                }
                            }
                    }
                    if (el) {
                        if (opts.length) {
                            opts.push(el);
                            el = formEls.container(['set','container','has-opts'], opts);
                        }
                        domEl._xElement = el;
                        els.push(el);
                    }
                }
            });
        }
        return els;
    };

    return _formBuilder;
});
