define(['./toastr'],function (toastr) {
    var highest_id = 100;
    var mediaPath = '/media';
    return {
        getId: () => {
            highest_id += Math.floor(Math.random() * 10)+1;
            return highest_id;
        },
        setMediaPath: (mp) => { mediaPath = mp; },
        toastr: toastr,
        urlToMageCode: (url) => {
            let parts = url.split('/');
            let key = parts.indexOf('___directive');
            if (key < 0) return false;
            let clean = parts[key+1].replace(/(%.*|,*)$/, '');
            return atob(clean);
        },
        imageUrls: (html) => {
            return html.replace(/::media::url::([^:]*?)::/g, mediaPath+'/$1');
        },
        xmlToHtml: (xml) => {
            let html = xml.replace(/<(\w*)(\s*)([^>]*)\/>/g, `<$1$2$3></$1>`);
            return html.replace(/<(br|hr|img)(\s*)(.*)><\/(br|hr|img)>/g, `<$1$2$3/>`);
        },
        toMageCode: (html) => {
            return html.replace(/("|')?::([^:]*?)::([^:]*?)::([^:]*?)::("|')?/g, `$1{{$2 $3="$4"}}$1`);
        },
        fromMageCode: (html, isValue) => {
            if (isValue) return html.replace(/{{(\w*)\s(\w*)="(.*?)"}}/g, `::$1::$2::$3::`);

            // Fix for previous Magento Editor
            html = html.replace(/&nbsp;&nbsp;/g, `&nbsp;`)
                .replace(/&nbsp;/g, ` `);
            return html.replace(/("|'){{(\w*)\s(\w*)="(.*?)"}}("|')/g, `"::$2::$3::$4::"`);
        },
        tryCleaning: (html) => {
            let div = document.createElement('div');
            div.innerHTML = html;
            return div.innerHTML;
        },
        sanitize: (val, html) => {
            val = val.replace(/&/g, `&amp;`)
                .replace(/</g, `&lt;`)
                .replace(/>/g, `&gt;`);

            return val.replace(/\\&lt;/g, `<`).replace(/\\&gt;/g, `>`);
        },
        desanitize: (val, html) => {
            if (html) val = val.replace(/(<|>)/g, `\\$1`);
            return val.replace(/&amp;/g, `&`)
                .replace(/&lt;/g, `<`)
                .replace(/&gt;/g, `>`);
        },
        /*
        *   @params:
        *       str: String value to parse
        *       first: Boolean return the first parsed value
        *       parseArr: Boolean parse names of value as arrays where applicable
        */
        parseXData: (str, first, parseArr) => {
            let pieces = str.split(';');
            let xData = {};
            pieces.some((piece) => {
                if (!piece.length) return;
                let nv = piece.split(':');
                if (nv.length >= 2) {
                    let name = nv.shift().trim();
                    let value = nv.join(':').trim();
                    /* Test if name could be array
                    * eg. "class-1=hide; class-2=show;"
                    */
                    let arr = name.split('-');
                    if (parseArr && arr.length > 1) {
                        name = arr[0];
                        if (!xData[name]) xData[name] = [];
                        if (isNaN(arr[1]*1)) throw "Must be number";
                        if (first) return xData = [name, arr[1]*1, value];
                        xData[name][arr[1]*1] = value;
                    } else {
                        if (first) return xData = [name, value];
                        xData[name] = value;
                    }
                } else throw "Invalid value";
            });
            return xData;
        }
    }
});
