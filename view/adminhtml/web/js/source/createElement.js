define(function () {
    return (nodeType, attrs, content) => {
        let el = document.createElement(nodeType);
        if (attrs && typeof attrs == 'object') {
            Object.keys(attrs).forEach(attr => {
                if (attrs[attr])
                    el.setAttribute(attr, attrs[attr]);
            });
        }
        if (content && typeof content == 'string') el.innerHTML = content;
        else if (Array.isArray(content)) {
            content.forEach(c => {
                el.appendChild(c);
            });
        }
        return el;
    };
});