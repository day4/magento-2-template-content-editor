define(['./createElement'],function (createEl) {
    return (classArr, children) => {
        let el = createEl('div', {class: classArr.join(' ')}, children);
        return el;
    };
});