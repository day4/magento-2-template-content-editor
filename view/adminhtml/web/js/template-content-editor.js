/**
 * Copyright © Day4Code. All rights reserved.
 * See COPYING.txt for license details.
 */
define(['jquery', './source/utils','./source/xApi','./source/createElement','./source/xSync','./source/formBuilder','./source/formElements','./source/codeEditorPopup','./source/xMessagePrompt'], function ($, utils, xApi, createEl, xSync, formBuilder, formEls, codeEditorPopup, xMessagePrompt) {
  return function(txtEl, opts) {
    if(!document.getElementById('tce-font')) {
      var link = document.createElement('link');
      link.id = 'tce-font';
      link.rel = 'stylesheet';
      link.href = 'https://fonts.googleapis.com/css?family=Alegreya+Sans:300,400,500,700';
      document.head.appendChild(link);
    }
    if(!document.getElementById('tce-icon-font')) {
      var link = document.createElement('link');
      link.id = 'tce-icon-font';
      link.rel = 'stylesheet';
      link.href = 'https://cdn.linearicons.com/free/1.0.0/icon-font.min.css';
      document.head.appendChild(link);
    }

    opts = opts || {};
    if (!txtEl) throw 'Invalid dom element given!';
    if (typeof txtEl == 'string') txtEl = document.getElementById(txtEl);
    if (!document.body.contains(txtEl)) throw 'Textarea not found in dom';

    let Scope = xSync.new(txtEl);
    xSync.setOpts(opts);

    function unique(str) { return str + '-' + Scope.getId(); }

    let tcForm = createEl('div', {class:'tce-tcf tce-view isvisible', id: unique('template-content-form')});
    let liveEl = createEl('div', {class:'tce-tcp tce-view', id: unique('template-content-preview')}, [createEl('iframe')]);

    let init = () => {
      let wrapper = createEl('div', {class:'tce', id: unique('template-content-editor')}, [tcForm, liveEl]);
      /* Contain all clicks within the tce */
      wrapper.onclick = (e) => {
        if (e.target.classList.contains('container')) {
          $('.container').not(e.target).removeClass('highlight');
          $(e.target).toggleClass('highlight');
        }
        e.stopPropagation();
        return true;
      };

      txtEl.classList.add('tce-view');
      txtEl.parentNode.insertBefore(wrapper, txtEl.nextSibling);
      txtEl.parentNode.insertBefore(viewToggles(), txtEl);

      Scope.setPreviewEl(liveEl);

      if (initCode = txtEl.value) _loadInput(initCode);
      else {
        tcForm.classList.add('syncing');
        xApi.findSlot(txtEl.id, true).then(xhtml => {
          _loadInput(xhtml);
        }).catch(err => {
          xApi.findSlot('tce_default', true).then(xhtml => {
            _loadInput(xhtml);
          }).catch(utils.toastr.error(err));
        });
      }

      txtEl.addEventListener('change', function (evt) {
        txtEl.classList.remove('tce-invalid');
        if (evt._tce_invoked) return;
        if (!_loadInput(txtEl.value)) txtEl.classList.add('tce-invalid');
      });
    };

    let _loadInput = (val, tried) => {
      tcForm.classList.add('syncing');
      let html = val;
      try {
        html = utils.fromMageCode(val);
        html = $.parseXML('<wrapper>'+html+'</wrapper>').children[0];
      } catch (e) {
        if (tried) {
          utils.toastr.warning('Invalid HTML syntax. Please check the code');
          tcForm.classList.remove('syncing');
          return false;
        } else {
          utils.toastr.info('Trying to cleaning code...');
          return _loadInput(utils.tryCleaning(val), true);
        }
      }

      try {
        Scope.setDOM(html);
        while(tcForm.firstChild) {
            tcForm.removeChild(tcForm.firstChild);
        }

        let forms = formBuilder(Scope.getDOM(), 0);
        if (!forms.length) {
          if ($(html).find('.x-template').length) utils.toastr.warning('x-template Exists but no editable elements were found!');
          else return confirm("Automatically create x-template?") && _loadInput('<div class="x-template">' + val + '</div>');
        } else {
          forms.forEach(el => {
            tcForm.appendChild(el);
          });
          Scope.update();
        }
      } catch (e) {
        utils.toastr.error(e.message || e);
      }
      tcForm.classList.remove('syncing');
      return true;
    };

    let viewToggles = () => {
      let editCodebtn = createEl('button', {class:'mage-btn white-btn lnr lnr-code'}, '');
      editCodebtn.onclick = (e) => {
        codeEditorPopup(txtEl.value, (_value) => {
          return _loadInput(_value);
        });
        return false;
      };

      return formEls.container(['tce', 'tce-views'], [
        editCodebtn,
        formEls.checkbox(['tce-toggle-form'], tcForm, 'Show form', 'isvisible'),
        formEls.checkbox(['tce-toggle-preview'], liveEl, 'Show preview', 'isvisible'),
        formEls.checkbox(['tce-toggle-preview-mobile'], liveEl, 'Mobile', 'mobile')
      ]);
    };

    init();
  };
});
